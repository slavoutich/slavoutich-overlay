# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Interception plugin for dual-function keys : Tap for one key, hold for another"
HOMEPAGE="https://gitlab.com/interception/linux/plugins/dual-function-keys"
SRC_URI="https://gitlab.com/interception/linux/plugins/dual-function-keys/-/archive/${PV}/${P}.tar.bz2"

DOCS=( README.md doc/examples.md )
LICENSE="MIT"

SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
app-misc/interception-tools
"

src_compile() {
	emake CC="$(tc-getCC)" CXX="$(tc-getCXX)" CFLAGS="${CFLAGS} -std=c99" CXXFLAGS="${CXXFLAGS} -std=c++11" PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake DESTDIR="${D}" CC="$(tc-getCC)" CXX="$(tc-getCXX)" CFLAGS="${CFLAGS} -std=c99" CXXFLAGS="${CXXFLAGS} -std=c++11" PREFIX="${EPREFIX}/usr" install
}
