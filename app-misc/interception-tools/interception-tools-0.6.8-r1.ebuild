# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit cmake systemd

DESCRIPTION="Small set of utilities for operating on input events of evdev
devices"

MY_P="${PN##interception-}-v${PV}"

HOMEPAGE="https://gitlab.com/interception/linux/tools"
SRC_URI="https://gitlab.com/interception/linux/tools/-/archive/v${PV}/${MY_P}.tar.bz2"

LICENSE="
	GPL-3
"

SLOT="0"
KEYWORDS="~amd64"

IUSE="systemd"

CDEPEND="
	dev-libs/libevdev
	virtual/libudev
	dev-cpp/yaml-cpp
	sys-libs/glibc
"
DEPEND="$CDEPEND"
RDEPEND="$CDEPEND"
BDEPEND="
	dev-libs/boost
	dev-util/cmake
"

S="${WORKDIR}/${MY_P}"

src_install() {
	cmake_src_install
	if use systemd; then
		systemd_dounit udevmon.service
	else
		newinitd "udevmon.init" "udevmon"
	fi
}

pkg_postinst() {
	elog "Before running udevmon, you will have to create and fill in"
	elog "/etc/interception/udevmon.yaml. You might also want to enable"
	elog "the udevmon init script."
}
