# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit systemd

DESCRIPTION="A key remapping daemon for linux"
HOMEPAGE="https://github.com/rvaiya/keyd"
SRC_URI="https://github.com/rvaiya/keyd/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
DEPEND="acct-group/keyd"
RDEPEND="$DEPEND"
IUSE="examples systemd"

src_prepare() {
	sed -i "/-groupadd keyd/d" Makefile || die
	sed -i "s@^PREFIX?=.*@PREFIX=${EPREFIX}/usr@g" Makefile || die
	sed -i "s/^COMMIT=.*//g" Makefile || die
	sed -i "s/^CFLAGS\:\=\-DVERSION\=.*/CFLAGS\:\=\-DVERSION\=\\\\\"v${PV}\\\\\" \\\\/g" Makefile || die
	default
}

src_install() {
	default

	rm -r "${D}"/usr/share/{man,doc} || die
	dodoc docs/{CHANGELOG.md,DESIGN.md}
	if use examples; then
		docompress -x /usr/share/doc/${PF}/examples
		dodoc -r examples
	fi

	if use systemd; then
		systemd_dounit keyd.service
	else
		newinitd "${FILESDIR}"/keyd.initd keyd
	fi
	insinto /etc/keyd
	doins "${FILESDIR}"/default.conf
	gzip -d data/{keyd,keyd-application-mapper}.1.gz || die
	doman data/{keyd,keyd-application-mapper}.1
}
